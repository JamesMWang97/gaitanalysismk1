#include <Arduino.h>
#include <Wire.h>
#include <Metro.h>
#include "SparkFun_BNO080_Arduino_Library.h"

/*PIN ASSIGNMENTS*/
#define M1_PIN 8 //Motor 1 MOSFET gate
#define M2_PIN 9 //Motor 2 MOSFET gate
#define B_LED_PIN 10 //Blue LED
#define G_LED1_PIN 11 //Green LED
#define G_LED2_PIN 12 //Green LED
#define R_FOOTSWITCH 14 //Big toe
#define Y_FOOTSWITCH 15 //Outer toes
#define B_FOOTSWITCH 22 //Heel
#define W_FOOTSWITCH 23 //Mid foot
#define BUZZ_PIN 13 //Buzzer speaker

/*RATES*/
#define DATA_RATE 10 //ms
#define I2C_RATE 100000 //Consider increasing this
#define BAUD_RATE 9600
#define PRINT_RATE 20 //ms

/*CONSTANTS*/
#define VIBE_ANALOG_VAL 100
#define FOOT_LOW_THRESH 300
#define FOOT_HIGH_THRESH 600

/*GYRO MODULE VARIABLES*/
float xGyro; //rad/s
float yGyro; //rad/s
float zGyro; //rad/s

float xAcc; //m/s^2
float yAcc; //m/s^2
float zAcc; //m/s^2

float quatI = 0;
float quatJ = 0;
float quatK = 0;
float quatReal = 0;
float yAngle = 0; //rad
float xAngle = 0; //rad
float zAngle = 0; //rad

/*FOOT SWITCH MODULE VARIABLES*/
int redFootVal = 0; //Big toe
int yellowFootVal = 0; //Outer toes
int blueFootVal = 0; //Heel
int whiteFootVal = 0; //Mid foot

/*STATE MACHINE SETTING VARIABLES*/
bool motor1Vibe = false;
int motor1Duration = 1000;
bool motor2Vibe = false;
int motor2Duration = 1000;
bool beeperBeep = false;
int beeperDuration = 1000;
int beeperFreq = 1000;
int lastHeelState = 0;
int lastToeState = 0;

/*FUNCTION PROTOTYPES*/
void setupPins(void);
void convertIMUData(void);
void printGyro(void);
void printAcc(void);
void printHeading(void);
void toggleBuzzerPWM(void);
void buzz(int,int);
void initSequence(void);
void vibrateMotor(int,int);
void beepBeeper(int,int);

/*ENABLE PRINTING*/
bool print = true;

BNO080 myIMU; //Create IMU instance
Metro printTimer = Metro(PRINT_RATE); //Controls rate of print to serial monitor
Metro m1Timer = Metro(motor1Duration);
Metro m2Timer = Metro(motor2Duration);
Metro beeperTimer = Metro(beeperDuration);
IntervalTimer pwmBuzzer; //Interrupt timer for using buzzer speaker

/*STATE MACHINE VARIABLE DEFINITIONS*/
typedef enum {
  M1_STANDBY,M1_VIBRATE
} Motor1States_t;

typedef enum {
  M2_STANDBY,M2_VIBRATE
} Motor2States_t;

typedef enum {
  BEEPER_STANDBY, BEEPER_BEEP
} BeeperStates_t;

typedef enum {
  HEEL_NOT_PRESSED,HEEL_PRESSED
} HeelSwitchStates_t;

typedef enum {
  TOE_NOT_PRESSED,TOE_PRESSED
} ToeSwitchStates_t;

typedef enum {
  MASTER_STANDBY,MASTER_HEEL_BEEP,MASTER_TOE_BEEP
} MasterStates_t;

Motor1States_t Motor1State;
Motor2States_t Motor2State;
BeeperStates_t BeeperState;
HeelSwitchStates_t HeelState;
ToeSwitchStates_t ToeState;
MasterStates_t MasterState;

void setup() {
  setupPins();
  Serial.begin(BAUD_RATE);
  if(print){ //If printing is enabled, waits for serial connection before proceeding
    while(!Serial){
      //wait
    }
    Serial.println("Serial Initialized...");
  }
  Wire.begin();
  myIMU.begin();
  Wire.setClock(I2C_RATE);
  //myIMU.enableGyro(DATA_RATE);
  //myIMU.enableAccelerometer(DATA_RATE);
  myIMU.enableRotationVector(DATA_RATE);
  initSequence();
  Motor1State = M1_STANDBY;
  Motor2State = M2_STANDBY;
  BeeperState = BEEPER_STANDBY;
  HeelState = HEEL_NOT_PRESSED;
  ToeState = TOE_NOT_PRESSED;
  MasterState = MASTER_STANDBY;
  lastHeelState = HeelState;
  lastToeState = ToeState;
}

void loop() {
  /*DATA ACQUISITION*/
  if (myIMU.dataAvailable() == true){
    quatI = myIMU.getQuatI();
    quatJ = myIMU.getQuatJ();
    quatK = myIMU.getQuatK();
    quatReal = myIMU.getQuatReal();
    //xGyro = myIMU.getGyroX();
    //yGyro = myIMU.getGyroY();
    //zGyro = myIMU.getGyroZ();
    //xAcc = myIMU.getAccelX();
    //yAcc = myIMU.getAccelY();
    //zAcc = myIMU.getAccelZ();
    convertIMUData();
  }
  //Foot analog read variables named after the color of the wire in the sensor
  redFootVal = analogRead(R_FOOTSWITCH); //Toe
  yellowFootVal = analogRead(Y_FOOTSWITCH);
  blueFootVal = analogRead(B_FOOTSWITCH); //Heel
  whiteFootVal = analogRead(W_FOOTSWITCH);

  /*PRINTING*/
  if(printTimer.check()){
    if(print){
      //printGyro();
      //printAcc();
      //printHeading();
      Serial.print(xAngle);
      Serial.print(",");
      Serial.println(HeelState);
    }
  }
  /*STATE MACHINES*/
  switch (MasterState){
    case MASTER_STANDBY:{
      if(lastHeelState != HeelState && lastHeelState == HEEL_NOT_PRESSED){
        beepBeeper(1000,250);
        MasterState = MASTER_HEEL_BEEP;
      }
      if(lastToeState != ToeState && lastToeState == TOE_NOT_PRESSED){
        beepBeeper(2000,250);
        MasterState = MASTER_TOE_BEEP;
      }
      lastHeelState = HeelState;
      lastToeState = ToeState;
    }
    break;

    case MASTER_HEEL_BEEP:{
      if(lastHeelState != HeelState && lastHeelState == HEEL_PRESSED){
        MasterState = MASTER_STANDBY;
      }
      lastHeelState = HeelState;
    }
    break;

    case MASTER_TOE_BEEP:{
      if(lastToeState != ToeState && lastToeState == TOE_PRESSED){
        MasterState = MASTER_STANDBY;
      }
      lastToeState = ToeState;
    }
    break;

  }

  switch (Motor1State){
    case M1_STANDBY:{
      if(motor1Vibe){
        motor1Vibe = false;
        analogWrite(M1_PIN,VIBE_ANALOG_VAL);
        m1Timer.interval(motor1Duration);
        m1Timer.reset();
        Motor1State = M1_VIBRATE;
      }
    }
    break;

    case M1_VIBRATE:{
      if(m1Timer.check()){
        analogWrite(M1_PIN,0);
        Motor1State = M1_STANDBY;
      }
    }
    break;
  }

  switch (Motor2State){
    case M2_STANDBY:{
      if(motor2Vibe){
        motor2Vibe = false;
        analogWrite(M2_PIN,VIBE_ANALOG_VAL);
        m2Timer.interval(motor2Duration);
        m2Timer.reset();
        Motor2State = M2_VIBRATE;
      }
    }
    break;

    case M2_VIBRATE:{
      if(m2Timer.check()){
        analogWrite(M2_PIN,0);
        Motor2State = M2_STANDBY;
      }
    }
    break;
  }

  switch (BeeperState){
    case BEEPER_STANDBY:{
      if(beeperBeep){
        beeperBeep = false;
        buzz(1, beeperFreq);
        beeperTimer.interval(beeperDuration);
        beeperTimer.reset();
        BeeperState = BEEPER_BEEP;
      }
    }
    break;

    case BEEPER_BEEP:{
      if(beeperTimer.check()){
        buzz(0,beeperFreq);
        BeeperState = BEEPER_STANDBY;
      }
    }
    break;
  }

  switch (HeelState){
    case HEEL_NOT_PRESSED:{
      if(blueFootVal < FOOT_LOW_THRESH){
        HeelState = HEEL_PRESSED;
      }
    }
    break;

    case HEEL_PRESSED:{
      if(blueFootVal > FOOT_HIGH_THRESH){
        HeelState = HEEL_NOT_PRESSED;
      }
    }
    break;
  }

  switch (ToeState){
    case TOE_NOT_PRESSED:{
      if(redFootVal < FOOT_LOW_THRESH){
        ToeState = TOE_PRESSED;
      }
    }
    break;

    case TOE_PRESSED:{
      if(redFootVal > FOOT_HIGH_THRESH){
        ToeState = TOE_NOT_PRESSED;
      }
    }
    break;
  }

}

/*FUNCTION DEFINITIONS*/

/*
Function: setupPins
Params: None
Returns: None
Description: Configures I/O pins for LEDs, buzzer speaker and motor control MOSFETs
*/
void setupPins(void){
  pinMode(B_LED_PIN,OUTPUT);
  pinMode(G_LED1_PIN,OUTPUT);
  pinMode(G_LED2_PIN,OUTPUT);
  pinMode(BUZZ_PIN,OUTPUT);
  pinMode(M1_PIN,OUTPUT);
  pinMode(M2_PIN,OUTPUT);
  pinMode(R_FOOTSWITCH,INPUT);
  pinMode(Y_FOOTSWITCH,INPUT);
  pinMode(B_FOOTSWITCH,INPUT);
  pinMode(W_FOOTSWITCH,INPUT);
  digitalWrite(M1_PIN,LOW); //Set MOSFET state
  digitalWrite(M2_PIN,LOW); //Set MOSFET state
  digitalWrite(BUZZ_PIN,LOW); //Set MOSFET state
}

/*
Function: convertIMUData
Params: None
Returns: None
Description: Converts quaternion angular positions into radians
*/
void convertIMUData(void){
  xAngle = atan2(2.0*(quatReal*quatI+quatJ*quatK),1.0-2.0*(pow(quatI,2.0)+pow(quatJ,2.0)));
  yAngle = asin(2.0*(quatReal*quatJ-quatK*quatI));
  zAngle = atan2(2.0*(quatReal*quatK+quatI*quatJ),1.0-2.0*(pow(quatJ,2.0)+pow(quatK,2.0)));
}

/*
Function: printGyro
Params: None
Returns: None
Description: Prints all gyro data
*/
void printGyro(void){
  Serial.print(xGyro);
  Serial.print(",");
  Serial.print(yGyro);
  Serial.print(",");
  Serial.println(zGyro);
}

/*
Function: printAcc
Params: None
Returns: None
Description: Prints all accelerometer data
*/
void printAcc(void){
  Serial.print(xAcc);
  Serial.print(",");
  Serial.print(yAcc);
  Serial.print(",");
  Serial.println(zAcc);
}

/*
Function: printHeading
Params: None
Returns: None
Description: Prints all heading data
*/
void printHeading(void){
  Serial.print(xAngle);
  Serial.print(",");
  Serial.print(yAngle);
  Serial.print(",");
  Serial.println(zAngle);
}

/*
Function: toggleBuzzerPWM
Params: None
Returns: None
Description: ISR to flip the buzzer controller pin
*/
void toggleBuzzerPWM(void){
  static int pinState = 0;
  digitalWrite(BUZZ_PIN,pinState);
  pinState = !pinState;
}

/*
Function: buzz
Params:
onBit - indicates whether buzzer should be on
freq - buzz frequency
Returns: None
Description: Beeps buzzer with BWM
*/
void buzz(int onBit, int freq){
  int micros = 1000000/(freq);
  if(onBit){
    pwmBuzzer.begin(toggleBuzzerPWM,micros);
  }else{
    pwmBuzzer.end();
    digitalWrite(BUZZ_PIN,LOW);
  }
}

/*
Function: initSequence
Params: None
Returns: None
Description: Beeps buzzer speaker and flashes lights to inidicate successful initialization
*/
void initSequence(void){
  const int DELAY = 110;
  const int BEEPS = 3;
  const int INIT_FREQ = 1000;
  const int FREQ_INC = 300;
  for(int i = 0; i < BEEPS; i++){
    digitalWrite(B_LED_PIN,HIGH);
    digitalWrite(G_LED1_PIN,HIGH);
    digitalWrite(G_LED2_PIN,HIGH);
    analogWrite(M1_PIN,VIBE_ANALOG_VAL);
    analogWrite(M2_PIN,VIBE_ANALOG_VAL);
    buzz(HIGH,INIT_FREQ + FREQ_INC*i);
    delay(DELAY);
    digitalWrite(B_LED_PIN,LOW);
    digitalWrite(G_LED1_PIN,LOW);
    digitalWrite(G_LED2_PIN,LOW);
    analogWrite(M1_PIN,0);
    analogWrite(M2_PIN,0);
    buzz(LOW,INIT_FREQ + FREQ_INC*i);
    delay(DELAY);
  }
  delay(3*DELAY);
  buzz(HIGH,INIT_FREQ);
  delay(3*DELAY);
  buzz(LOW,INIT_FREQ);
  delay(3*DELAY);
  buzz(HIGH,INIT_FREQ + FREQ_INC*(BEEPS-1));
  delay(3*DELAY);
  buzz(LOW,INIT_FREQ + FREQ_INC);
}

/*
Function: vibrateMotor
Params:
motorNum - 1 or 2 to indicate motor to vibrate
vibeDuration - duration of vibration in ms
Returns: None
Description: Vibrates specified motor channel
*/
void vibrateMotor(int motorNum,int vibeDuration){
  if(motorNum == 1){
    motor1Vibe = true;
    motor1Duration = vibeDuration;
  }else if(motorNum == 2){
    motor2Vibe = true;
    motor2Duration = vibeDuration;
  }
}

/*
Function: beepBeeper
Params:
freq - frequency of beep tone
beepDuration - duration of beep in ms
Returns: None
Description: Beeps beeper speaker at specified frequency and duration
*/
void beepBeeper(int freq, int beepDuration){
  beeperFreq = freq;
  beeperDuration = beepDuration;
  beeperBeep = true;
}